package paginapher

import (
	"testing"
	"fmt"
)

type intSlice []int

func (i intSlice) Len() int { return len(i) }
func (i intSlice) SubSliceToEnd(start int) SliceInterface { return i[start:] }
func (i intSlice) SubSlice(start, end int) SliceInterface { return i[start:end] }

func TestNew(t *testing.T) {
	p := New(nil)
	if p.TotalPages() != 0 { t.Errorf("Total pages incorrect") }
	if p.CurrentPage() != 1 { t.Errorf("Current page incorrect") }
	if len(p.Data()) != 0 { t.Errorf("Data fetch length incorrect") }
}

func TestFifty(t *testing.T) {
	fiftyInts := make([]interface{}, 50)
	for i:=0; i < 50; i++ {
		fiftyInts[i] = i
	}
	p := New(fiftyInts)
	if p.TotalPages() != 3 { t.Errorf("Total pages incorrect") }
	if p.CurrentPage() != 1 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != p.NumberPerPage() { t.Errorf("Data fetch length incorrect") }
	for i := 0; uint(i) < p.NumberPerPage(); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
	t.Log("Print page 2")
	p.SetCurrentPage(2)
	if p.TotalPages() != 3 { t.Errorf("Total pages incorrect") }
	if p.CurrentPage() != 2 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != p.NumberPerPage() { t.Errorf("Data fetch length incorrect") }
	for i := 0; uint(i) < p.NumberPerPage(); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
}

func TestFourty(t *testing.T) {
	fiftyInts := make([]interface{}, 40)
	for i:=0; i < 40; i++ {
		fiftyInts[i] = i
	}
	p := New(fiftyInts)
	if p.TotalPages() != 2 { t.Errorf("Total pages incorrect") }
	if p.CurrentPage() != 1 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != p.NumberPerPage() { t.Errorf("Data fetch length incorrect") }
	for i := 0; uint(i) < p.NumberPerPage(); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
	t.Log("Print page 2")
	p.SetCurrentPage(2)
	if p.TotalPages() != 2 { t.Errorf("Total pages incorrect") }
	if p.CurrentPage() != 2 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != p.NumberPerPage() { t.Errorf("Data fetch length incorrect") }
	for i := 0; i < len(p.Data()); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
}

func TestFortyOne(t *testing.T) {
	fiftyInts := make([]interface{}, 41)
	for i:=0; i < 41; i++ {
		fiftyInts[i] = i
	}
	p := New(fiftyInts)
	if p.TotalPages() != 3 { t.Errorf(fmt.Sprintf("Total pages incorrect: %d", p.TotalPages())) }
	if p.CurrentPage() != 1 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != p.NumberPerPage() { t.Errorf("Data fetch length incorrect") }
	for i := 0; uint(i) < p.NumberPerPage(); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
	t.Log("Print page 3")
	p.SetCurrentPage(3)
	if p.TotalPages() != 3 { t.Errorf("Total pages incorrect") }
	if p.CurrentPage() != 3 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != 1 { t.Errorf("Data fetch length incorrect") }
	for i := 0; i < len(p.Data()); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
}

func TestTwentyThreeOnSevenPerPage(t *testing.T) {
	fiftyInts := make([]interface{}, 23)
	for i:=0; i < 23; i++ {
		fiftyInts[i] = i
	}
	p := New(fiftyInts)
	p.SetNumberPerPage(7)
	if p.TotalPages() != 4 { t.Errorf(fmt.Sprintf("Total pages incorrect: %d", p.TotalPages())) }
	if p.CurrentPage() != 1 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != p.NumberPerPage() { t.Errorf("Data fetch length incorrect") }
	for i := 0; i < len(p.Data()); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
	t.Log("Print page 4")
	p.SetCurrentPage(4)
	if p.TotalPages() != 4 { t.Errorf("Total pages incorrect") }
	if p.CurrentPage() != 4 { t.Errorf("Current page incorrect") }
	if uint(len(p.Data())) != 2 { t.Errorf("Data fetch length incorrect") }
	for i := 0; i < len(p.Data()); i++ {
		t.Log(fmt.Sprintf("%g", p.Data()[i].(int)))
	}
}