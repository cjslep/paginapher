package paginapher

import (
	"math"
)

type Paginator interface {
	UpdateData(dataObjs SliceInterface)
	SetNumberPerPage(numPerPage int)
	NumberPerPage() int
	TotalPages() int
	CurrentPage() int
	SetCurrentPage(curr int)
	Data() SliceInterface
}

type SliceInterface interface {
	Len() int
	SubSliceToEnd(start int) SliceInterface
	SubSlice(start, end int) SliceInterface
}

type paginapher struct {
	dataStore SliceInterface
	numberPerPage int
	currPage int
}

func New(data SliceInterface) Paginator {
	return &paginapher {
		data,
		100,
		1,
	}
}

func (p *paginapher) UpdateData(dataObjs SliceInterface) {
	p.dataStore = dataObjs
}

func (p *paginapher) SetNumberPerPage(numPerPage int) {
	p.numberPerPage = numPerPage
}

func (p *paginapher) NumberPerPage() int {
	return p.numberPerPage
}

func (p *paginapher) TotalPages() int {
	res := math.Ceil(float64(p.dataStore.Len()) / float64(p.numberPerPage))
	return int(res)
}

func (p *paginapher) CurrentPage() int {
	return p.currPage
}

func (p *paginapher) SetCurrentPage(curr int) {
	if curr >= 1 && curr <= p.TotalPages() {
		p.currPage = curr
	}
}

func (p *paginapher) Data() SliceInterface {
	if (p.dataStore != nil) {
		top := (p.currPage - 1) * p.numberPerPage + p.numberPerPage
		if top > p.dataStore.Len() {
			return p.dataStore.SubSliceToEnd((p.currPage - 1) * p.numberPerPage)
		} else {
			return p.dataStore.SubSlice((p.currPage - 1) * p.numberPerPage, top)
		}
	} else {
		return nil
	}
}